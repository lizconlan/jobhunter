require 'rails_helper'

describe JobsController do
  let(:company) {
    co = FactoryBot.create(:company, location: 'San Francisco')
    FactoryBot.create(:job, company: co)
    co
  }

  let(:job) { company.jobs.first }

  describe '#create' do
    it 'assigns the expected company' do
      post :create, params: { company_slug: company.slug,
                              job: FactoryBot.attributes_for(:job) }
      expect(assigns[:company]).to match(company)
    end

    context 'valid data is passed in' do
      it 'creates a new job record' do
        post :create, params: { company_slug: company.slug,
                                job: FactoryBot.attributes_for(:job) }
        expect(assigns[:job]).to be_persisted
      end

      it 'redirects to the parent company page' do
        post :create, params: { company_slug: company.slug,
                                job: FactoryBot.attributes_for(:job) }
        expect(response).to redirect_to(company_path(company.slug))
      end
    end

    context 'invalid data is passed in' do
      it 'does not save the new job data' do
        post :create,
             params: { company_slug: company.slug,
                       job: FactoryBot.attributes_for(:job, title: nil) }
        expect(assigns[:job]).to be_new_record
      end

      it 'renders the new template' do
        post :create,
             params: { company_slug: company.slug,
                       job: FactoryBot.attributes_for(:job, title: nil) }
        expect(response).to render_template('new')
      end
    end
  end

  describe '#show' do
    it 'assigns the expected company' do
      get :show, params: { id: job.id, company_slug: company.slug }
      expect(assigns[:company]).to match(company)
    end

    it 'assigns the expected job' do
      get :show, params: { id: job.id, company_slug: company.slug }
      expect(assigns[:job]).to match(job)
    end
  end

  describe '#update'
end
