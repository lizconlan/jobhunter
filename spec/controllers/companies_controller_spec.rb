require 'rails_helper'

describe CompaniesController do
  let(:company_1) { FactoryBot.create(:company, location: 'Paris') }
  let(:company_2) { FactoryBot.create(:company, location: 'Chicago') }

  describe '#index' do
    before { get :index }

    let!(:hiring_now) { FactoryBot.create(:company, :with_job) }
    let!(:applied_to) { FactoryBot.create(:company, :with_job_application) }

    it 'assigns a list of companies that have no advertised positions' do
      expect(assigns[:not_hiring]).to match_array([company_1, company_2])
    end

    it 'assigns a list of companies you have applied to' do
      expect(assigns[:applied]).to match_array([applied_to])
    end

    it 'assigns a list of companies with current advertised positions' do
      expect(assigns[:hiring]).to match_array([hiring_now])
    end
  end

  describe '#show' do
    it 'assigns the expected company' do
      get :show, params: { slug: company_1.slug }
      expect(assigns[:company]).to match(company_1)
    end
  end

  describe '#create' do
    context 'valid data is passed in' do
      it 'creates a new company record' do
        post :create,
             params: { company: FactoryBot.attributes_for(:company) }
        expect(assigns[:company]).to be_persisted
      end

      it 'redirects to the companies index' do
        post :create,
             params: { company: FactoryBot.attributes_for(:company) }
        expect(response).to redirect_to(companies_path)
      end
    end

    context 'invalid data is passed in' do
      let(:obj_params) { FactoryBot.attributes_for(:company, location: nil) }

      it 'does not save the new company data' do
        post :create,params: { company: obj_params }
        expect(assigns[:company]).to be_new_record
      end

      it 'renders the new template' do
        post :create, params: { company: obj_params }
        expect(response).to render_template('new')
      end
    end
  end

  describe '#edit' do
    it 'assigns the expected company' do
      get :edit, params: { slug: company_1.slug }
      expect(assigns[:company]).to match(company_1)
    end
  end

  describe '#update' do
    it 'assigns the expected company' do
      patch :update, params: { slug: company_1.slug, company: { name: 'Test'} }
      expect(assigns[:company]).to match(company_1)
    end

    context 'invalid data is passed in' do
      it 'does not update the new company data' do
        patch :update, params: { slug: company_1.slug, company: { name: nil } }
        expect(assigns[:company].name).to_not eq(nil)
      end

      it 'renders the show template' do
        patch :update, params: { slug: company_1.slug, company: { name: nil } }
        expect(response).to render_template('edit')
      end
    end
  end
end
