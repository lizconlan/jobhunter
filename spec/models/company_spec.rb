# == Schema Information
#
# Table name: companies
#
#  id              :bigint           not null, primary key
#  name            :string
#  location        :string
#  remote_friendly :boolean
#  notes           :string
#  pros            :string
#  cons            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  slug            :string
#

require 'rails_helper'

describe Company do
  describe '.recent_first' do
    subject { described_class.recent_first }
    let(:companies) { 3.times.map { FactoryBot.create(:company) } }

    it { is_expected.to eq companies.reverse }
  end

  describe '.hiring' do
    subject { described_class.hiring }

    let!(:hiring_now) { FactoryBot.create(:company, :with_job) }
    let!(:not_hiring_now) { FactoryBot.create(:company) }

    it { is_expected.to include(hiring_now) }
    it { is_expected.to_not include(not_hiring_now) }
  end

  describe '.not_hiring' do
    subject { described_class.not_hiring }

    let!(:hiring_now) { FactoryBot.create(:company, :with_job) }
    let!(:not_hiring_now) { FactoryBot.create(:company) }

    it { is_expected.to_not include(hiring_now) }
    it { is_expected.to include(not_hiring_now) }
  end

  describe '.have_applied' do
    subject { described_class.have_applied }

    let!(:not_applied) { FactoryBot.create(:company, :with_job) }
    let!(:not_hiring) { FactoryBot.create(:company) }
    let!(:applied) { FactoryBot.create(:company, :with_job_application) }

    it { is_expected.to include(applied) }
    it { is_expected.to_not include(not_hiring) }
    it { is_expected.to_not include(not_applied) }
  end

  describe '.have_not_applied' do
    subject { described_class.have_not_applied }

    let!(:not_applied) { FactoryBot.create(:company, :with_job) }
    let!(:not_hiring) { FactoryBot.create(:company) }
    let!(:applied) { FactoryBot.create(:company, :with_job_application) }

    it { is_expected.to include(not_applied) }
    it { is_expected.to include(not_hiring) }
    it { is_expected.to_not include(applied) }
  end

  describe '.new' do
    it 'requires a name' do
      company = Company.new(location: 'test')
      expect(company.valid?).to eq(false)
      expected_error = company.errors.messages[:name].first
      expect(expected_error).to eq("can't be blank")
    end

    it 'requires the name to be unique' do
      Company.create(name: 'test', location: 'test')
      company = Company.new(name: 'test', location: 'test')
      expect(company.valid?).to eq(false)
      expected_error = company.errors.messages[:name].first
      expect(expected_error).to eq("has already been taken")
    end

    it 'requires a location' do
      company = Company.new(name: 'test')
      expect(company.valid?).to eq(false)
      expected_error = company.errors.messages[:location].first
      expect(expected_error).to eq("can't be blank")
    end
  end

  describe '#save' do
    let(:company) { FactoryBot.build(:company, name: 'Test slug generation!') }

    it 'sets the slug' do
      company.save
      expect(company.slug).to eq 'test-slug-generation'
    end
  end
end
