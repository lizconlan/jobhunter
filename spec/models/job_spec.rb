# == Schema Information
#
# Table name: jobs
#
#  id               :bigint           not null, primary key
#  company_id       :integer
#  title            :string
#  salary           :string
#  days_remote      :integer
#  where_found      :string
#  notes            :string
#  apply_by         :datetime
#  date_applied     :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  application_link :string
#

require 'rails_helper'

describe Job do
  let(:company) { FactoryBot.create(:company, location: 'here') }

  describe '.new' do
    it 'requires a title' do
      job = Job.new
      expect(job.valid?).to eq(false)
      expected_error = job.errors.messages[:title].first
      expect(expected_error).to eq("can't be blank")
    end
  end

  describe '.closing_soon' do
    subject { described_class.closing_soon }

    let!(:no_closing_date) { FactoryBot.create(:job, company: company) }

    let!(:closing_later) do
      FactoryBot.create(:job, company: company,
                              apply_by: Time.zone.now + 2.weeks)
    end

    let!(:closing_tomorrow) do
      FactoryBot.create(:job, company: company,
                              apply_by: Time.zone.now + 1.day)
    end

    let!(:closing_soon) do
      FactoryBot.create(:job, company: company,
                              apply_by: Time.zone.now + 7.days)
    end

    it { is_expected.to include(closing_tomorrow) }
    it { is_expected.to include(closing_soon) }
    it { is_expected.to_not include(closing_later) }
    it { is_expected.to_not include(no_closing_date) }
  end

  describe '.have_applied' do
    subject { described_class.have_applied }

    let!(:not_applied) { FactoryBot.create(:job, company: company) }

    let!(:applied) do
      FactoryBot.create(:job, company: company,
                              date_applied: Time.zone.now - 2.weeks)
    end

    it { is_expected.to include(applied) }
    it { is_expected.to_not include(not_applied) }
  end

  describe '#applied_for?' do
    subject { job.applied_for? }

    let(:job) { FactoryBot.create(:job, company: company) }

    context 'no date_applied is set' do
      it { is_expected.to eq false }
    end

    context 'date_applied has been set' do
      it 'returns true' do
        job.update(date_applied: Date.yesterday)
        expect(subject).to eq(true)
      end
    end
  end
end
