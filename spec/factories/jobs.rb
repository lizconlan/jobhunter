# == Schema Information
#
# Table name: jobs
#
#  id               :bigint           not null, primary key
#  company_id       :integer
#  title            :string
#  salary           :string
#  days_remote      :integer
#  where_found      :string
#  notes            :string
#  apply_by         :datetime
#  date_applied     :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  application_link :string
#

FactoryBot.define do
  factory :job do
    sequence(:title) { |n| "Test Role #{n}" }
  end

  trait :have_applied do
    date_applied { Time.zone.now - 1.day }
  end
end
