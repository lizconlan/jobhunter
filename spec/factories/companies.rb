# == Schema Information
#
# Table name: companies
#
#  id              :bigint           not null, primary key
#  name            :string
#  location        :string
#  remote_friendly :boolean
#  notes           :string
#  pros            :string
#  cons            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  slug            :string
#

FactoryBot.define do
  factory :company do
    sequence(:name) { |n| "Test Company #{n}" }
    location { 'London' }
  end

  trait :with_job do
    after(:create) do |company, _|
      create(:job, company: company)
    end
  end

  trait :with_job_application do
    after(:create) do |company, _|
      create(:job, :have_applied, company: company)
    end
  end
end
