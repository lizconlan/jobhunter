class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.integer :company_id
      t.string :title
      t.string :salary
      t.integer :days_remote
      t.string :where_found
      t.string :notes

      t.timestamp :apply_by
      t.timestamp :date_applied

      t.timestamps
    end
  end
end
