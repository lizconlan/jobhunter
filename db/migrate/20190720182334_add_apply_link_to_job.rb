class AddApplyLinkToJob < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :application_link, :string
  end
end
