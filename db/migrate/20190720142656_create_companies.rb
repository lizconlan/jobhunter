class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :location
      t.boolean :remote_friendly
      t.string :notes
      t.string :pros
      t.string :cons

      t.timestamps
    end
  end
end
