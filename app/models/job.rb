# == Schema Information
#
# Table name: jobs
#
#  id               :bigint           not null, primary key
#  company_id       :integer
#  title            :string
#  salary           :string
#  days_remote      :integer
#  where_found      :string
#  notes            :string
#  apply_by         :datetime
#  date_applied     :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  application_link :string
#

class Job < ApplicationRecord
  belongs_to :company

  validates_presence_of :title

  scope :closing_soon,
        -> { where(apply_by: Time.zone.now..Time.zone.now + 7.days) }
  scope :have_applied, -> { where("date_applied IS NOT NULL") }

  def applied_for?
    !date_applied.blank?
  end
end
