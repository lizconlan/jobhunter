# == Schema Information
#
# Table name: companies
#
#  id              :bigint           not null, primary key
#  name            :string
#  location        :string
#  remote_friendly :boolean
#  notes           :string
#  pros            :string
#  cons            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  slug            :string
#

class Company < ApplicationRecord
  has_many :jobs

  scope :recent_first, -> { order(created_at: :desc) }
  scope :hiring,
        -> { joins(:jobs).where('jobs.company_id > 0') }
  scope :not_hiring,
        -> { left_outer_joins(:jobs).where( jobs: { id: nil } )}
  scope :have_applied, -> { joins(:jobs).merge(Job.have_applied) }
  scope :have_not_applied, -> { where.not(id: have_applied) }

  validates_presence_of :name
  validates_presence_of :location
  validates_uniqueness_of :name

  before_save :update_slug, if: proc { |company| company.name_changed? }

  private

  def update_slug
    self.slug = name.downcase.gsub(' ', '-').gsub(/[,;&!?#]/, '')
  end
end
