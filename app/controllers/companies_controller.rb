class CompaniesController < ApplicationController
  before_action :set_company, only: [:edit, :show, :update]

  def index
    @applied = Company.have_applied.recent_first
    @not_hiring = Company.not_hiring.recent_first
    @hiring = Company.hiring.have_not_applied.recent_first
  end

  def show
  end

  def new
  end

  def create
    @company = Company.new(company_params)
    if @company.save
      redirect_to companies_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    @company.update(company_params)
    if @company.save
      redirect_to company_path
    else
      render :edit
    end
  end

  private

  def set_company
    @company = Company.find_by(slug: params[:slug])
  end

  def company_params
    params.require(:company).
      permit(:name, :location, :notes, :remote_friendly, :pros, :cons)
  end
end
