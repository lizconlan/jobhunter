class JobsController < ApplicationController
  before_action :set_company
  before_action :set_job, only: [:show, :edit, :update]

  def new
  end

  def create
    @job = Job.new(job_params)
    @job.company_id = @company.id
    if @job.save
      redirect_to company_path(@company.slug)
    else
      render :new
    end
  end

  def show
  end

  def update
    @job.update(job_params)
    @job.company_id = @company.id
    if @job.save
      redirect_to company_job_path(@company.slug, @job.id)
    else
      render :edit
    end
  end

  private

  def job_params
    params.require(:job).
      permit(:title, :salary, :days_remote, :where_found, :apply_by, :notes,
             :date_applied, :application_link)
  end

  def set_company
    @company = Company.find_by(slug: params[:company_slug])
  end

  def set_job
    @job = Job.find(params[:id])
  end
end
