Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'general#index'

  resources :companies, param: :slug do
    resources :jobs, only: [:new, :create, :edit, :show, :update]
  end
end
